<?php

/**
 * @file
 * Common functions for the References Integrity.
 */

/**
 * Get the list of referential integrity behaviors.
 *
 * @param $field_entities
 *   You can currently use either t('nodes') or t('users').
 */
function references_integrity_get_behavior_options($field_entities) {
  return array(
    REFERENCES_INTEGRITY_BEHAVIOR_NONE => t('None.'),
    REFERENCES_INTEGRITY_BEHAVIOR_REMOVE => t('Delete also field items when referenced @entities are deleted.', array('@entities' => $field_entities)),
  );
}

/**
 * Get the RI behavior defined for the given field name.
 */
function references_integrity_get_field_behavior($field_name) {
  return variable_get('references_integrity_behavior_' . $field_name, '');
}

/**
 * Get information about reference fields of the given type.
 *
 * @param $field_type
 *   Field types supported are 'node_reference' and 'user_reference'.
 */
function references_integrity_get_reference_fields($field_type, $counters = TRUE) {
  static $entity_info = array();
  $fields = array();

  foreach (field_info_instances() as $entity_type => $entity) {
    if (!isset($entity_info[$entity_type])) {
      $entity_info = entity_get_info($entity_type);
    }
    foreach ($entity as $bundle_name => $bundle) {
      foreach ($bundle as $field_name => $field) {
        $field += field_info_field($field_name);
        $field += array(
          'entity_name' => $entity_info['label'],
          'bundle_name' => $entity_info['bundles'][$bundle_name]['label'],
        );
        if ($field['type'] == $field_type) {
          $fields[] = references_integrity_get_field_info($field, $counters);
        }
      }
    }
  }

  return $fields;
}

/**
 * Get orphans related information about a single field.
 *
 * @param $field
 *   The field to retrieve information from.
 */
function references_integrity_get_field_info($field, $counters = TRUE) {
  if (empty($field) || empty($field['type']) || !in_array($field['type'], references_integrity_references())) {
    return FALSE;
  }
  $counters_array = array('count' => 0, 'orphan' => 0, 'status' => 'ok');

  if ($counters) {
    $id = references_integrity_get_id_key($field['type']);

    $query = new EntityFieldQuery;
    $query->entityCondition('entity_type', $field['entity_type']);
    $query->entityCondition('bundle', $field['bundle']);
    $query->fieldCondition($field['field_name'], $id, 0, '>');
    $result = $query->execute();

    $total_count = $orphan_count = 0;
    if (isset($result[$field['entity_type']])) {
      $total_count = count($result[$field['entity_type']]);

      list($table, $field_info) = each($field['storage']['details']['sql'][FIELD_LOAD_CURRENT]);
      list($column, ) = each($field_info);

      $references = array();
      foreach (entity_load($field['entity_type'], array_keys($result[$field['entity_type']])) as $entity_id => $entity) {
        $name = $field['field_name'];
        foreach ($entity->{$name} as $language_code => $delta_group) {
          foreach ($delta_group as $delta => $delta_item) {
            $references[] = $delta_item[$column];
          }
        }
      }

      $referenced_nids = array_keys(node_load_multiple($references));

      $orphan_count = 0;
      foreach ($references as $reference) {
        if (!in_array($reference, $referenced_nids)) {
          $orphan_count++;
        }
      }

      $counters_array = array(
        'count' => $total_count,
        'orphan' => $orphan_count,
        'status' => $orphan_count > 0 ? 'warning' : 'ok',
      );
    }
  }

  return array(
    'entity_type' => $field['entity_type'],
    'entity_name' => $field['entity_name'],
    'bundle' => $field['bundle'],
    'bundle_name' => $field['bundle_name'],
    'field_name' => $field['field_name'],
    'field_type' => $field['type'],
    'has_delta' => (!empty($field['multiple']) ? TRUE : FALSE),
    'label' => $field['label'],
    'ri_behavior' => references_integrity_get_field_behavior($field['field_name']),
  ) + $counters_array;
}

/**
 * Apply referential integrity rule to the given object id.
 *
 * @param $field_type
 *   Field types supported are 'node_reference' and 'user_reference'.
 * @param $field_value
 *   A node id (nid) or user id (uid).
 */
function references_integrity_apply($field_type, $object) {

  $id = references_integrity_get_id_key($field_type);

  $queue = DrupalQueue::get('references_integrity');
  $queue->createQueue();

  // Get all references field instances.
  foreach (references_integrity_get_reference_fields($field_type, FALSE) as $field) {

    // Process only if the admin enforced referential integrity.
    if ($field['ri_behavior'] == REFERENCES_INTEGRITY_BEHAVIOR_REMOVE) {
      $query = new EntityFieldQuery;
      $query->entityCondition('entity_type', $field['entity_type']);
      $query->entityCondition('bundle', $field['bundle']);
      $query->fieldCondition($field['field_name'], $id, $object->{$id}, '=');
      $result = $query->execute();

      if (isset($result[$field['entity_type']])) {
        foreach (entity_load($field['entity_type'], array_keys($result[$field['entity_type']])) as $entity_id => $entity) {
          $item = array(
            'entity_type' => $field['entity_type'],
            'entity_id' => $entity_id,
            'field_name' => $field['field_name'],
            'column' => $id,
            'references' => array($object->{$id}),
          );

          // Queue the item for cron processing.
          $queue->createItem($item);
        }
      }
    }
  }
}
